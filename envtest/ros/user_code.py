#!/usr/bin/python3

import math
from yaml import DirectiveToken
import numdifftools as nd

from pickle import NONE
from utils import AgileCommandMode, AgileCommand
# from rl_example import rl_example

ARRAY_Z_INDEX = 2


def compute_command_vision_based(state, img):
    ################################################
    # !!! Begin of user code !!!
    # TODO: populate the command message
    ################################################
    print("Computing command vision-based!")
    # print(state)
    # print("Image shape: ", img.shape)

    # Example of SRT command
    command_mode = 0
    command = AgileCommand(command_mode)
    command.t = state.t
    command.rotor_thrusts = [1.0, 1.0, 1.0, 1.0]

    # Example of CTBR command
    command_mode = 1
    command = AgileCommand(command_mode)
    command.t = state.t
    command.collective_thrust = 15.0
    command.bodyrates = [0.0, 0.0, 0.0]

    # Example of LINVEL command (velocity is expressed in world frame)
    command_mode = 2
    command = AgileCommand(command_mode)
    command.t = state.t
    command.velocity = [1.0, 0.0, 0.0]
    command.yawrate = 0.0

    ################################################
    # !!! End of user code !!!
    ################################################

    return command


def __get_n_nearest_obstacles(obstacles, n):
    obstacles_with_distance_list = []
    distance_with_scale = None

    for obstacle in obstacles:
        obstacle_x = obstacle.position.x
        obstacle_y = obstacle.position.y * 1.7
        obstacle_z = obstacle.position.z * 1.7
        if obstacle_x > 1:
            #distance = obstacle_x + abs(obstacle_y) + abs(obstacle_z)
            distance = math.sqrt(obstacle_x**2 + abs(obstacle_y**2) + abs(obstacle_z**2))
            distance_with_scale = distance - obstacle.scale
            obstacle_with_distance = [distance_with_scale, obstacle]
            obstacles_with_distance_list.append(obstacle_with_distance)
    
    obstacles_with_distance_list.sort(key=lambda y: y[0])
    return obstacles_with_distance_list[:n]


def __get_direction_arrow(obstacle):
    left_speed = 2
    up_speed = 2
    # we are inside AND we are over the obstacle
    if obstacle.position.z < 0:
        # if z is smaller than scale
        if obstacle.position.y < 0:
            # go on left-up
            return [0, left_speed, up_speed]
        elif obstacle.position.y > 0:
            # go on right-up
            return [0, -left_speed, up_speed]
    # we are inside AND under the obstacle
    elif obstacle.position.z > 0:
        # if z is smaller than scale
        if obstacle.position.y < 0:
            # go on left-down
            return [0, left_speed, -up_speed]
        elif obstacle.position.y > 0:
            # go on right-down
            return [0, -left_speed, -up_speed]
    return [0, 0, 0]


def compute_command_state_based(state, obstacles, rl_policy=None):
    # print(state)
    # print("Obstacles: ", obstacles.obstacles)

    # Example of SRT command
    # command_mode = 0
    # command = AgileCommand(command_mode)
    # command.t = state.t
    # command.rotor_thrusts = [1.0, 1.0, 1.0, 1.0]

    # Example of CTBR command
    # command_mode = 1
    # command = AgileCommand(command_mode)
    # command.t = state.t
    # command.collective_thrust = 10.0
    # command.bodyrates = [0.0, 0.0, 0.0]

    # Example of LINVEL command (velocity is expressed in world frame)
    command_mode = 2
    command = AgileCommand(command_mode)
    command.t = state.t
    command.yawrate = 0.0

    nearest_3_obstacles = __get_n_nearest_obstacles(obstacles.obstacles, 5)

    final_speed = [0, 0, 0]
    total_distance = 0
    for distance, obstacle in nearest_3_obstacles:
        direction_arrow = __get_direction_arrow(obstacle)
        y = direction_arrow[1]/(distance*2)
        y = y/1.6
        z = direction_arrow[2]/(distance*2)
        z = z/1.5
        direction_arrow = [0, y, z]
        final_speed = list(map(lambda x,y: x+y, final_speed, direction_arrow))
        total_distance = total_distance + distance

    forward_speed = 2.5 + total_distance*0.25
    final_speed = [forward_speed, final_speed[1], final_speed[2]]
    
    floor_safe_distance = 1.1
    if floor_safe_distance > state.pos[ARRAY_Z_INDEX]:
            # print("distance")
            final_speed = [forward_speed, final_speed[1], 2]

    command.velocity = final_speed

    return command
