#!/usr/bin/python3

import math
from yaml import DirectiveToken
import numdifftools as nd
from scipy.spatial import KDTree

from pickle import NONE
from utils import AgileCommandMode, AgileCommand
# from rl_example import rl_example



def compute_command_vision_based(state, img):
    ################################################
    # !!! Begin of user code !!!
    # TODO: populate the command message
    ################################################
    print("Computing command vision-based!")
    # print(state)
    # print("Image shape: ", img.shape)

    # Example of SRT command
    command_mode = 0
    command = AgileCommand(command_mode)
    command.t = state.t
    command.rotor_thrusts = [1.0, 1.0, 1.0, 1.0]

    # Example of CTBR command
    command_mode = 1
    command = AgileCommand(command_mode)
    command.t = state.t
    command.collective_thrust = 15.0
    command.bodyrates = [0.0, 0.0, 0.0]

    # Example of LINVEL command (velocity is expressed in world frame)
    command_mode = 2
    command = AgileCommand(command_mode)
    command.t = state.t
    command.velocity = [1.0, 0.0, 0.0]
    command.yawrate = 0.0

    ################################################
    # !!! End of user code !!!
    ################################################

    return command


obs = {}

def f (pos):
    global obs
    asdf = pos[1]**2 + (pos[2]-1)**2

    # print("before", asdf)
    for i in range(obs.num):
        asdf += Oj(obs.obstacles[i], pos)

    # print("after", asdf)

    return asdf

Aj = 10

def Oj (object, pos):
    var = Aj * math.exp(- 
        # ((pos[0] - object.position.x) **2 + (pos[1] - object.position.y) **2 + (pos[2] - object.position.y) **2) 
        ((object.position.x) **2 + (object.position.y) **2 + (object.position.z) **2) 
        / ((2*object.scale)**2)
    )

    return var

kv = 1
kgrad = 0.5


def compute_command_state_based(state, obstacles, rl_policy=None):
    global obs
    ################################################
    # !!! Begin of user code !!!
    # TODO: populate the command message
    ################################################
    print("Computing command based on obstacle information!")
    # print(state)
    # print("Obstacles: ", obstacles.obstacles)
    
    position = state.pos
    obs = obstacles
    herbert = f(position)
    print(position)
    print(herbert)
    herbertgradient = nd.Gradient(f)(position)
    print(herbertgradient)

    t = [-herbertgradient[1], herbertgradient[0], -herbertgradient[2]]

    velocity = -kgrad * herbert * herbertgradient + t + [5,0,0]

    print(velocity)

    # Example of SRT command
    # command_mode = 0
    # command = AgileCommand(command_mode)
    # command.t = state.t
    # command.rotor_thrusts = [1.0, 1.0, 1.0, 1.0]
 
    # # Example of CTBR command
    # command_mode = 1
    # command = AgileCommand(command_mode)
    # command.t = state.t
    # command.collective_thrust = 10.0
    # command.bodyrates = [0.0, 0.0, 0.0]

    # Example of LINVEL command (velocity is expressed in world frame)
    command_mode = 2
    command = AgileCommand(command_mode)
    command.t = state.t
    command.velocity = velocity
    # command.velocity = [1,0,0]
    command.yawrate = 0.0

    # If you want to test your RL policy
    # if rl_policy is not None:
        # command = rl_example(state, obstacles, rl_policy)

    ################################################
    # !!! End of user code !!!
    ################################################

    return command
